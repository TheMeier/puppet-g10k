# g10k

[![Puppet Forge](http://img.shields.io/puppetforge/v/landcareresearch/g10k.svg)](https://forge.puppetlabs.com/landcareresearch/g10k)
[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_PuppetG10k_Build%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_PuppetG10k_Build&guest=1)

## Description

A module to manage [g10k](https://github.com/xorpaul/g10k) for linux based systems.

## Limitations

Debian Based Systems.

## Usage

A script is installed to /usr/local/bin called g10k.bash which includes the configuration specified in this module.
Use g10k.bash to run g10k.

### Example

```bash
    g10k.bash
```
